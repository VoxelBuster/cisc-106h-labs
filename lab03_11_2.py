from cisc106 import assertEqual


def map(fcn, par2_list):
    """
    Takes a list and applies a helper function to each element.
    The helper function must take one parameter.
    """
    if not par2_list:
        return []
    else:
        return [fcn(par2_list[0])] + map(fcn, par2_list[1:])


def scale_helper(n):
    """
    Returns n scaled up by a factor of 5.
    """
    return n * 5


def square_helper(n):
    """
    Returns the square of n.
    """
    return n * n


def square(par1_list):
    """
    Takes a list and multiplies each element by itself.
    Only numbers are allowed as list elements.
    """
    if not par1_list:
        return []
    else:
        return [par1_list[0] * par1_list[0]] + square(par1_list[1:])


def scale_by_five(par1_list):
    """
    Takes a list and multiplies each element by 5.
    Only numbers are allowed as list elements.
    """
    if not par1_list:
        return []
    else:
        return [par1_list[0] * 5] + scale_by_five(par1_list[1:])


assertEqual(square([5, 10, 25]), [25, 100, 625])
assertEqual(square([0, -1, 200]), [0, 1, 40000])
assertEqual(square([]), [])
assertEqual(scale_by_five([5, 10, 25]), [25, 50, 125])
assertEqual(scale_by_five([0, -1, 200]), [0, -5, 1000])
assertEqual(scale_by_five([]), [])

assertEqual(map(scale_helper, [5, 10, 25]), [25, 50, 125])
assertEqual(map(scale_helper, [0, -1, 200]), [0, -5, 1000])
assertEqual(map(scale_helper, []), [])
assertEqual(map(square_helper, []), [])
assertEqual(map(square_helper, [-2, -4, 8]), [4, 16, 64])
assertEqual(map(square_helper, [-1, 0, 1, 0.5]), [1, 0, 1, 0.25])
