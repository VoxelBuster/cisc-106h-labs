from cisc106 import assertEqual


def memory(n, m, alist):
    """
    Replaces an element in alist with m if either of the 2 elements before it were equal to n.
    """
    list_length = len(alist)
    elts_to_change = []
    for i in range(list_length):
        if (i - 2 >= 0 and alist[i - 2] == n) or (i - 1 >= 0 and alist[i - 1] == n):
            elts_to_change += [i]
    for elt_index in elts_to_change:
        alist[elt_index] = m
    return alist


assertEqual(memory(5, 7, [5, 5, 10]), [5, 7, 7])
assertEqual(memory(42, 0, []), [])
assertEqual(memory(11, 12, [11, 11]), [11, 12])
assertEqual(memory(0, 1, [2, 2, 2, 2, 0, 2, 2, 0, 0, 2, 0]), [2, 2, 2, 2, 0, 1, 1, 0, 1, 1, 1])
assertEqual(memory(5, 7, [5, 5, 6, 3, 5, 2, 3, 4, 5]), [5, 7, 7, 7, 5, 7, 7, 4, 5])
