from cisc106 import assertEqual
from matplotlib import pyplot
import random
import timeit


def merge(asorted, bsorted):
    """
    Merges two sorted lists in ascending order into one sorted list.
    """
    if not asorted:
        return bsorted
    elif not bsorted:
        return asorted
    else:
        if asorted[0] < bsorted[0]:
            return [asorted[0]] + merge(asorted[1:], bsorted)
        else:
            return [bsorted[0]] + merge(asorted, bsorted[1:])


assertEqual(merge([], []), [])
assertEqual(merge([1, 2, 3], []), [1, 2, 3])
assertEqual(merge([], [4, 5, 6]), [4, 5, 6])
assertEqual(merge([7, 2, 5, 0], [3, 3, 10]), [0, 2, 3, 3, 5, 7, 10])


def merge_sort(alist):
    """
    Sorts alist in ascending using a recursive merge sort algorithm.
    """
    if len(alist) <= 1:
        return alist
    else:
        mid = len(alist) // 2
        return merge(merge_sort(alist[:mid]), merge_sort(alist[mid:]))


assertEqual(merge_sort([]), [])
assertEqual(merge_sort([3]), [3])
assertEqual(merge_sort([5, 3, 7, 5, 1]), [1, 3, 5, 5, 7])
assertEqual(merge_sort([-1, 0, 0, 2, 10, 3.14, -4, -7]), [-7, -4, -1, 0, 0, 2, 3.14, 10])
assertEqual(merge_sort([16, -9, -32, 4, 38, 21, 37, -6]), [-32, -9, -6, 4, 16, 21, 37, 38])
assertEqual(merge_sort([-39, -1, -9, -45, 40, 6, 18, 38]), [-45, -39, -9, -1, 6, 18, 38, 40])

alist = list(range(500))
random.shuffle(alist)

dependent = []
range_obj = range(100, 550, 50)
for list_length in range_obj:
    short_list = alist[:list_length]
    elapsed = timeit.timeit("merge_sort(short_list)", number=1000, globals=globals())
    dependent += [elapsed]
pyplot.plot(range_obj, dependent, 'r', label="merge_sort (recursive)")

pyplot.title('Merge Sort Timings')
pyplot.xlabel('List Size')
pyplot.ylabel('Time (seconds)')
pyplot.legend()
pyplot.savefig('lab07_9.png')
pyplot.show()
