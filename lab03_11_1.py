from cisc106 import assertEqual


def square(par1_list):
    """
    Takes a list and multiplies each element by itself.
    Only numbers are allowed as list elements.
    """
    if not par1_list:
        return []
    else:
        return [par1_list[0] * par1_list[0]] + square(par1_list[1:])


def scale_by_five(par1_list):
    """
    Takes a list and multiplies each element by 5.
    Only numbers are allowed as list elements.
    """
    if not par1_list:
        return []
    else:
        return [par1_list[0] * 5] + scale_by_five(par1_list[1:])


assertEqual(square([5, 10, 25]), [25, 100, 625])
assertEqual(square([0, -1, 200]), [0, 1, 40000])
assertEqual(square([]), [])
assertEqual(scale_by_five([5, 10, 25]), [25, 50, 125])
assertEqual(scale_by_five([0, -1, 200]), [0, -5, 1000])
assertEqual(scale_by_five([]), [])
