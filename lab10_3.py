# Derik Spedden, Galen Nare, and Will Quintana
from cisc106 import assertEqual


def fits(nlist1, nlist2):
    """
    takes two 2D nested lists with the same size and shape and returns true if every boolean value in one has its complement in the other
    """

    for elt1 in range(len(nlist1)):
        for elt2 in range(len(nlist1[elt1])):
            if bool(nlist1[elt1][elt2]) != bool(nlist2[elt1][elt2]):
                if (not bool(nlist1[elt1][elt2])) == bool(nlist2[elt1][elt2]):
                    result = True
                else:
                    return False
            else:
                return False
    return result


assertEqual(fits([[True, False], [False, False]], [[False, True], [True, True]]), True)
assertEqual(fits([[0, 0], [1, 0]], [[1, 1], [0, 1]]), True)
assertEqual(fits([[[0], []], [[], []]], [[[], [0]], [[0], [0]]]), True)
assertEqual(fits([[True, False], [True, True]], [[False, True], [True, True]]), False)
