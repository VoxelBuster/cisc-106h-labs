def boolTable1():
    """
    Prints the truth table for p1 and (p2 or p3)
    """
    print('p1\t\tp2\t\tp3\t\tp1 and (p2 or p3)')
    bools = [True, False]
    for p1 in bools:
        for p2 in bools:
            for p3 in bools:
                print(str(p1) + '\t' + str(p2) + '\t' + str(p3) + '\t' + str(p1 and (p2 or p3)))
