function newPic = drawGN(pic_filename, x, y, size, layer, newValue)
  % read the image
  newPic = imread(pic_filename);
  % Draw G
  for row = y:y + size
    newPic(row, x, layer) = newValue;
  endfor
  for col = x:x + size
    newPic(y, col, layer) = newValue;
    newPic(y + size, col, layer) = newValue;
  endfor
  for row = idivide(y + size, 2):y + size
    newPic(row, x + size, layer) = newValue;
  endfor
  for col = idivide(x + size, 2):x + size
    newPic(idivide(y + size, 2), col, layer) = newValue;
  endfor
  % Draw N
  for row = y:y + size
    newPic(row, x + size * 2, layer) = newValue;
    newPic(row, x + size * 3, layer) = newValue;
  endfor
  for row = y:y + size
      newPic(row, row + size * 2, layer) = newValue;
  endfor
  % write the image
  imwrite(newPic, strcat(pic_filename, '.png'));
endfunction
