from cisc106 import assertEqual


def sum_list(par1_list):
    """
	Adds all of the numbers in a list together and returns the sum.
    """
    if not par1_list:
        return 0  # return zero for empty list
    else:
        return par1_list[0] + sum_list(par1_list[1:])  # add first element to the sum of the next elements


assertEqual(sum_list([]), 0)
assertEqual(sum_list([0]), 0)
assertEqual(sum_list([1, 2, 3]), 6)
