from lab07_6 import insertion_sort as insertion_sort1
from lab07_7 import insertion_sort as insertion_sort2
from matplotlib import pyplot
import random
import timeit

alist = list(range(500))
random.shuffle(alist)

dependent = []
range_obj = range(100, 550, 50)
for list_length in range_obj:
    short_list = alist[:list_length]
    elapsed = timeit.timeit("insertion_sort1(short_list)", number=1000, globals=globals())
    dependent += [elapsed]
pyplot.plot(range_obj, dependent, 'r', label="insertion_sort (recursive)")

dependent = []
for list_length in range_obj:
    short_list = alist[:list_length]
    elapsed = timeit.timeit("insertion_sort2(short_list)", number=1000, globals=globals())
    dependent += [elapsed]
pyplot.plot(range_obj, dependent, 'b', label="insertion_sort (looped)")

pyplot.title('Insertion Sort Timings')
pyplot.xlabel('List Size')
pyplot.ylabel('Time (seconds)')
pyplot.legend()
pyplot.savefig('lab07_8.png')
pyplot.show()

