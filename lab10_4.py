# Derik Spedden, Galen Nare, and Will Quintana
from cisc106 import assertEqual


def transpose(amatrix):
    """
    Transposes a 2-dimensional list (amatrix) across its first diagonal.
    """
    if not amatrix:
        return amatrix
    row_len = len(amatrix[0])
    new_matrix = []
    for col in range(row_len):
        new_row = []
        for row in amatrix:
            new_row += [row[col]]
        new_matrix += [new_row]
    return new_matrix


assertEqual(transpose([]), [])
assertEqual(transpose([[1, 2], [3, 4]]), [[1, 3], [2, 4]])
assertEqual(transpose([[2, 4, 3], [5, 8, 1]]), [[2, 5], [4, 8], [3, 1]])
