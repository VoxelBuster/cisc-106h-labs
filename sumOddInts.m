function result = sumOddInts(start, finish)
  sum = 0;
  if mod(start, 2) == 0
    start = start + 1;
  end
  for current = start:2:finish
    sum = sum + current;
  end
  result = sum;