from cisc106 import assertEqual


def insert_in_order(n, asorted):
    """
    Places n into a sorted list in ascending order.
    """
    if not asorted:
        return [n]
    else:
        if n > asorted[0]:
            return [asorted[0]] + insert_in_order(n, asorted[1:])
        else:
            return [n] + asorted


assertEqual(insert_in_order(1, []), [1])
assertEqual(insert_in_order(4, [1, 2, 3, 5, 6]), [1, 2, 3, 4, 5, 6])
assertEqual(insert_in_order(-9, [-12, -10, -1, 0, 7]), [-12, -10, -9, -1, 0, 7])


def insertion_sort(alist):
    """
    Sorts a list in ascending order. Has n^2 complexity.
    """
    if len(alist) <= 1:
        return alist
    else:
        return insert_in_order(alist[0], insertion_sort(alist[1:]))


assertEqual(insertion_sort([]), [])
assertEqual(insertion_sort([5]), [5])
assertEqual(insertion_sort([5, 3, 7, 5, 1]), [1, 3, 5, 5, 7])
assertEqual(insertion_sort([-7, -1, 0, 0, 2, 3.14, -4, 10]), [-7, -4, -1, 0, 0, 2, 3.14, 10])
