from cisc106 import assertEqual


def reverser(a, b):
    """
    Adds two lists together from parameters in reverse order.
    """
    return b + [a]


def accumulate(fcn, par2_list, init):
    """
    Accumulates a list based on a helper function
    """
    if not par2_list:
        return init
    else:
        return fcn(par2_list[0], accumulate(fcn, par2_list[1:], init))


assertEqual(accumulate(reverser, [], []), [])
assertEqual(accumulate(reverser, [1, 2, 3, 4], []), [4, 3, 2, 1])
assertEqual(accumulate(reverser, [17, 32, 'hi', 0, [], False], []), [False, [], 0, 'hi', 32, 17])
