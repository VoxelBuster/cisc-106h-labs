from cisc106 import assertEqual


def negative(n, par2_list):
    """
    Checks if a number is negative and returns it if it is.
    Returns empty list if it is not negative
    """
    if n < 0:
        return [n] + par2_list
    else:
        return [] + par2_list


def accumulate(fcn, par2_list, init):
    """
    Compares the items in the list and returns the result based on the function parameter.
    The init parameter should be the first element in the list.
    """
    if not par2_list:
        return init
    else:
        return fcn(par2_list[0], accumulate(fcn, par2_list[1:], init))


assertEqual(accumulate(negative, [], []), [])
assertEqual(accumulate(negative, [5, 6, 7, 8], []), [])
assertEqual(accumulate(negative, [1, 4, -2, -7, 8, -42], []), [-2, -7, -42])
