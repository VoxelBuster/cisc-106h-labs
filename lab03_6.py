from cisc106 import assertEqual


def replace_n(to_replace, replacement, par2_list, n):
    """
    Replaces up to n occurrences of to_replace with replacement in par2_list
    """
    if not par2_list:
        return []
    elif n == 0:
        return par2_list
    else:
        if par2_list[0] == to_replace:
            return [replacement] + replace_n(to_replace, replacement, par2_list[1:], n - 1)
        else:
            return [par2_list[0]] + replace_n(to_replace, replacement, par2_list[1:], n)


assertEqual(replace_n(5, 7, [], 40), [])
assertEqual(replace_n(2, 3, [1, 2, 3, 4], 0), [1, 2, 3, 4])
assertEqual(replace_n(7, 13, [0, 7, 7, 7, 7, 12], 2), [0, 13, 13, 7, 7, 12])
