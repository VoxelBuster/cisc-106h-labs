from cisc106 import assertEqual


def swap(a, b, alist):
    """
    Swaps elements at indexes a and b in a list and returns the new list.
    """
    temp = alist[a]
    alist[a] = alist[b]
    alist[b] = temp
    return alist


def insertion_sort(alist):
    """
    Sorts a list in ascending order in-place. Has n^2 complexity.
    """
    if len(alist) <= 1:
        return alist
    for i in range(1, len(alist)):
        j = i
        while j > 0 and alist[j - 1] > alist[j]:
            swap(j, j - 1, alist)
            j -= 1
    return alist


assertEqual(insertion_sort([]), [])
assertEqual(insertion_sort([3]), [3])
assertEqual(insertion_sort([5, 3, 7, 5, 1]), [1, 3, 5, 5, 7])
assertEqual(insertion_sort([-1, 0, 0, 2, 10, 3.14, -4, -7]), [-7, -4, -1, 0, 0, 2, 3.14, 10])
assertEqual(insertion_sort([16, -9, -32, 4, 38, 21, 37, -6]), [-32, -9, -6, 4, 16, 21, 37, 38])
assertEqual(insertion_sort([-39, -1, -9, -45, 40, 6, 18, 38]), [-45, -39, -9, -1, 6, 18, 38, 40])
