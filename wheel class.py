from cisc106 import assertEqual


class Wagon:
    def __init__(self, numWheels):
        self.numWheels = numWheels
        self.location = 0

    def move(self):
        """
        Wagon may only move to the right
        """
        self.location += 1


'''
Tests
>w1 = Wagon(4)
>w1.location == 0
True
>w1.move()
>w1.location == 1
True
'''
