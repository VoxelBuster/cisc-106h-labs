function newPic = pixelCircle(centerX, centerY, radius, pic, layer, newValue, thickness)   
  newPic = pic;
  for y = 1:rows(newPic)
    for x = 1:columns(newPic)
      if isWithinTolerance((x - centerX).^2 + (y - centerY).^2, radius, thickness / 4)
        newPic(x, y, layer) = newValue;
      end
    end
  end
end