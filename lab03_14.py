from cisc106 import assertEqual


def lt(a, b):
    """
    Helper function that returns True if a is less than b
    """
    return a < b


def splice(asorted, bsorted):
    """
    Sorts two pre-sorted lists in ascending order.
    Works only on lists with numbers.
    """
    if not asorted:
        return bsorted
    elif not bsorted:
        return asorted
    else:
        if lt(asorted[0], bsorted[0]):
            return [asorted[0]] + splice(asorted[1:], bsorted)
        else:
            return [bsorted[0]] + splice(asorted, bsorted[1:])


assertEqual(splice([], []), [])
assertEqual(splice([], [1]), [1])
assertEqual(splice([2], []), [2])
assertEqual(splice([7, 8], [5, 6]), [5, 6, 7, 8])
assertEqual(splice([0, 7, 9, 21, 32], [-2, 7, 7, 10, 14, 78]), [-2, 0, 7, 7, 7, 9, 10, 14, 21, 32, 78])
