from cisc106 import assertEqual


def sum_list_iter(par1_list, par2_sum):
    """
    Adds all numbers in a list together to a given state variable.
    Items in the list may only be numbers.
    The state variable (par2_sum) may be nonzero.
    """
    if not par1_list:
        return par2_sum  # end of list, return the state and stop.
    else:
        return sum_list_iter(par1_list[1:],
                             par1_list[0] + par2_sum)  # iterate throught the list and add the next element


assertEqual(sum_list_iter([1, 2, 3], 0), 6)
assertEqual(sum_list_iter([], 1), 1)
assertEqual(sum_list_iter([16, 16, 16, 16], 64), 128)
