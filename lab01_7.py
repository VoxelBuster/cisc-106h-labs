# Galen Nare and Patrick Cronin

from cisc106 import assertEqual


def average(a, b):
    return (a + b) / 2  # returns the average of a and b


assertEqual(average(2, 4), 3)
assertEqual(average(0, 1), 0.5)
assertEqual(average(10, 30), 20)
