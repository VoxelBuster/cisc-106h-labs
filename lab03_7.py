from cisc106 import assertEqual


def sum(a, b):
    """
    Adds a and b and returns the result
    """
    return a + b


def accumulate(fcn, par2_list, init):
    """
    Accumulates a list (par1_list) of numbers using a helper function fcn.
    """
    if not par2_list:
        return init
    else:
        return fcn(par2_list[0], accumulate(fcn, par2_list[1:], init))


assertEqual(accumulate(sum, [], 0), 0)
assertEqual(accumulate(sum, [6, 6, 6], 0), 18)
assertEqual(accumulate(sum, [-1, 1, 3, 6], 0), 9)
