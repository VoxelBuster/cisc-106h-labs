from cisc106 import assertEqual


def evens_n(n):
    """
    Returns a list of even numbers from 0 to n
    """
    result = []
    for i in range(0, n + 1):
        if i % 2 == 0:
            result += [i]
    return result


assertEqual(evens_n(0), [0])
assertEqual(evens_n(5), [0, 2, 4])
assertEqual(evens_n(24), [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24])
