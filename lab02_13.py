from cisc106 import assertEqual


def my_replace(to_replace, replacement, par3_list):
    """
    Replaces the first matched item in par3_list with the replacement.
    Does not replace more than once.
    This function does not care about the types of the list elements but
    will not check nested lists.
    """
    if not par3_list:
        return []  # end of list. stop.
    elif par3_list[0] == to_replace:
        return [replacement] + par3_list[1:]  # item found, replace it.
    else:
        return [par3_list[0]] + my_replace(to_replace, replacement,
                                           par3_list[1:])  # item not found, iterate the list and check next item


assertEqual(my_replace(2, 4, [1, 2, 3, 4]), [1, 4, 3, 4])
assertEqual(my_replace(3, 7, [1, 1, 2, 2, 3, 3]), [1, 1, 2, 2, 7, 3])
assertEqual(my_replace(0, 1, [0, 0, 0]), [1, 0, 0])
assertEqual(my_replace(100, 400, []), [])
