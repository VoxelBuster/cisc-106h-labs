# -*- coding: utf-8 -*-
from cisc106 import assertEqual

class Grid:
    """
    Has attributes numRows, numCols, and squares (a 2D list containing True/False).
    """
    def __init__(self,rows,cols,squares):
        self.numRows = rows
        self.numCols = cols
        self.squares = squares if squares else [cols * [False] for r in range(rows)]

def generate_all_locations(grid, shape):
    """Takes a single shape in one position and finds all the places it could fit on the
    grid, given its dimensions.
    Returns: a list of row,col tuples"""
    shape_height = len(shape.squares)
    shape_width = len(shape.squares[0])
    result = []
    for row in range(len(grid.squares)):
        for column in range(len(grid.squares[row])):
            if len(grid.squares) >= (row + shape_height) and len(grid.squares[row]) >= (column + shape_width):
                result += [(row,column)]
    return result
    
def get_valid_locations(location_list, grid, shape):
    """Returns list of locations where shape does not overlap shapes
    already on grid. Assumes that all locations in parameter list are
    potential fits for this shape.
    Calls: fits"""
    validlocation = []

def fits(location, grid, shape):
    """Returns True if shape placed at location does not overlap shapes
    already on grid.location: row,col tuple"""
    for row in range(location[0][0], (location[0][0] + len(shape))):
        for column in range(location[0][1], (location[0][0] + len(shape[row]))):
            if grid[row][column] == shape[row][column] == False:
                return False
    return True
    
class Shape:
    """A "shape" is a nested tuple, where the real world shape is
    represented as ones. For example, an "L" shape could be
    represented as((False, False, True),(True, True, True))
    Has attributes x, y, letter, squares (a nested list of type boolean),color, num_rotations"""

    def __init__(self, letter, squares, color):
        self.x = 0  # will be modified later to indicate position
        self.y = 0  # will be modified later to indicate position2
        self.letter = letter
        self.squares = squares
        self.color = color
        self.num_rotations = 0

    def get_shape(letter):
            """Returns the Shape corresponding to the letter parameter: I for a line; T for a T;
            L for an L on its back, foot to right;
            Z for a Z. More may be added. """
    L = Shape('L', ((False, False, True), (True, True, True)), 'red')
    Z = Shape('Z', ((True, True, False), (False, True, True)), 'blue')
    I = Shape('I', ((True, True, True, True)), 'yellow')
    T = Shape('T', ((True, True, True), (False, True, False)), 'green')
    if 'L' == letter:
        return L
    elif 'Z' == letter:
        return Z
    elif 'I' == letter:
        return I
    elif 'T' == letter:
        return T

assertEqual(get_valid_locations([(0,0),(0,1)], Grid(2,4,[]), get_shape(’L’)),[(0,0),(0,1)])