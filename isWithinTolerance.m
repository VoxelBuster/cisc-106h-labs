function result = isWithinTolerance(num1, num2, tolerance)
  if abs(num1 - num2) < tolerance
    return true
  else
    return false
  end
end