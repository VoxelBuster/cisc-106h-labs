from cisc106 import assertEqual


def greater_than_five(n, par2_list):
    """
    Returns a single element list of n when n is greater than 5.
    """
    if n > 5:
        return [n] + par2_list
    else:
        return [] + par2_list5


def accumulate(fcn, par2_list, init):
    """
    Compares the items in the list and returns the result based on the function parameter.
    The init parameter should be the first element in the list.
    """
    if not par2_list:
        return init
    else:
        return fcn(par2_list[0], accumulate(fcn, par2_list[1:], init))


assertEqual(accumulate(greater_than_five, [], []), [])
assertEqual(accumulate(greater_than_five, [-1, 2, 4.9], []), [])
assertEqual(accumulate(greater_than_five, [5, 5.1, 0, -3.14, 10], []), [5.1, 10])
