from cisc106 import assertEqual


def findAll(key, alist):
    """
    Returns a list of indexes that key appears at in alist.
    """
    elt_indexes = []
    for i in range(len(alist)):
        if key == alist[i]:
            elt_indexes += [i]
    return elt_indexes


assertEqual(findAll('apple', []), [])
assertEqual(findAll(7, [7, 1, 2, 7, 3, 4, 7, 8]), [0, 3, 6])
assertEqual(findAll(True, [True, False, False, False, True, False, True]), [0, 4, 6])
