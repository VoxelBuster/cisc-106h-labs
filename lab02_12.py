from cisc106 import assertEqual


def nested_sum(par1_list):
    """
    Adds all of the numbers in a list and returns the sum.
    This function does not care if some of the numbers are nested inside of list,
    given that all elements are numbers or other lists.
    """
    if not par1_list:
        return 0  # end of list. stop.
    else:
        if isinstance(par1_list[0], list):
            return nested_sum(par1_list[0]) + nested_sum(par1_list[1:])  # get the nested sum of an inner list
        else:
            return par1_list[0] + nested_sum(par1_list[1:])  # add the next item in the list


assertEqual(nested_sum([1, 3, [4, [], [2, 1]]]), 11)
assertEqual(nested_sum([[], [5, 5, 5, [10, 5]]]), 30)
assertEqual(nested_sum([]), 0)
