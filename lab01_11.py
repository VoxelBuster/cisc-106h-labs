# Galen Nare and Patrick Cronin

from cisc106 import assertEqual


def validNumber(n):  # check if number should be kept in sum
    if 10 <= n <= 14:  # return 0 if number is between 10 and 14 inclusive
        return 0
    elif 25 <= n <= 29:  # return 0 if number is between 25 and 29 inclusive
        return 0
    else:
        return n  # if the above conditions are not met return the number given


def sumSpecials(a, b, c, d):
    return validNumber(a) + validNumber(b) + validNumber(c) + validNumber(
        d)  # sum all the parameters after they have been validated


assertEqual(sumSpecials(1, 2, 3, 4), 10)  # test that sumSpecials and by extension, validNumber, works properly
assertEqual(sumSpecials(2, 5, 10, 15), 22)  # checking the 10 <= n <= 14 condition
assertEqual(sumSpecials(26, 20, 12, 1), 21)  # checking the 25 <= n <= 29 condition
