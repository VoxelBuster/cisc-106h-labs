import random
import sys


def merge(asorted, bsorted):
    """
    Sorts two pre-sorted lists in ascending order.
    Works only on lists with numbers.
    """
    merged = []
    if len(bsorted) > len(asorted):
        for i in range(len(asorted)):
            if asorted[i] < bsorted[i]:
                merged += [asorted[i], bsorted[i]]
            else:
                merged += [bsorted[i], asorted[i]]
    else:
        for i in range(len(bsorted)):
            if asorted[i] < bsorted[i]:
                merged += [asorted[i], bsorted[i]]
            else:
                merged += [bsorted[i], asorted[i]]
    return merged


def merge_sort(alist):
    if len(alist) <= 1:
        return alist
    fragments = [[x] for x in alist]  # smash the list into atoms
    while len(fragments) > 1:  # keep merging until no more merging can be done
        merged_once = []
        for i in range(0, len(fragments), 2):
            if i + 1 < len(fragments):
                merged_once += merge(fragments[i], fragments[i + 1])
        if len(merged_once) % 2 > 0 and len(merged_once) > 1:
            merged_once[-2:-1] = [merge(merged_once[-2], merged_once[-1])]
        fragments = merged_once
    return fragments[0]


limit = 10000
randlist = list(range(limit))
sys.setrecursionlimit(2 * limit)
random.shuffle(randlist)
print(merge_sort(randlist))
