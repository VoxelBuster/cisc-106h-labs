import random
import timeit

from matplotlib import pyplot

from cisc106 import assertEqual


def min1(alist):
    """ precondition: alist is not empty """
    if len(alist) == 1:
        return alist[0]
    else:
        min_rest = min1(alist[1:])
        if alist[0] < min_rest:
            return alist[0]
        else:
            return min_rest


assertEqual(min1([1, 2, 3]), 1)
assertEqual(min1([2, 1, 3]), 1)
assertEqual(min1([2, 3, 1]), 1)


def min2(alist):
    """ precondition: alist is not empty """
    min_so_far = alist[0]
    for elt in alist:
        if elt < min_so_far:
            min_so_far = elt
    return min_so_far


assertEqual(min2([1, 2, 3]), 1)
assertEqual(min2([2, 1, 3]), 1)
assertEqual(min2([2, 3, 1]), 1)


def min3(alist):
    """ precondition: alist is not empty """

    def helper(elt, min_of_rest):
        if elt < min_of_rest:
            return elt
        else:
            return min_of_rest

    import functools
    return functools.reduce(helper, alist, alist[0])


assertEqual(min3([1, 2, 3]), 1)
assertEqual(min3([2, 1, 3]), 1)
assertEqual(min3([2, 3, 1]), 1)

alist = list(range(500))
random.shuffle(alist)

dependent = []
range_obj = range(50, 501, 50)
for size in range_obj:
    short_list = alist[:size]
    min_elapsed_1 = timeit.timeit('min1(short_list)', number=1000, globals=globals())
    dependent = dependent + [min_elapsed_1]
pyplot.plot(range_obj, dependent, 'r', label="min1")
dependent = []
for size in range_obj:
    short_list = alist[:size]
    min_elapsed_2 = timeit.timeit('min2(short_list)', number=1000, globals=globals())
    dependent = dependent + [min_elapsed_2]
pyplot.plot(range_obj, dependent, 'g', label="min2")
dependent = []
for size in range_obj:
    short_list = alist[:size]
    min_elapsed_3 = timeit.timeit('min3(short_list)', number=1000, globals=globals())
    dependent = dependent + [min_elapsed_3]
pyplot.plot(range_obj, dependent, 'b', label="min3")
dependent = []
for size in range_obj:
    short_list = alist[:size]
    min_elapsed_builtin = timeit.timeit('min(short_list)', number=1000, globals=globals())
    dependent = dependent + [min_elapsed_builtin]
pyplot.plot(range_obj, dependent, 'y', label="min (builtin)")
pyplot.title('Min Function Timings')
pyplot.xlabel('Array Size')
pyplot.ylabel('Time (seconds)')
pyplot.legend()
pyplot.savefig('lab06.png')
pyplot.show()
