from cisc106 import assertEqual


def read_to_dict(filename):
    """
    Reads a data file of key-value pairs and collects them in a dictionary. Returns a dictionary.
    """
    weights = {}
    with open(filename) as infile:
        data = infile.readline()
        while data:
            pair = data.split()
            weights[pair[0]] = pair[1]
            data = infile.readline()

    return weights


assertEqual(read_to_dict('someData.txt'),
            {'moose,': '1500', 'cat,': '13', 'wombat,': '40', 'mouse,': '0.1', 'Corinthian_Grey,': '1200'})
